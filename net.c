#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "rdnsd.h"
#include "llist.h"

typedef struct _sendq_t {
    char *buf;
    size_t len;
    struct sockaddr_in sin;
    struct llhead sendq_list;
} sendq_t;

static LL_HEAD(sendq_list_head);
static int sendq_count = 0;
static int net_fd = -1;

/* know when to quit */
extern int exit_code;

/* boolean to know when to update the default resolver */
extern int update_resolver;

static int _net_recv(void) {
    struct sockaddr_in sin;
    socklen_t len;
    char buf[MAX_PAYLOAD];
    ssize_t r;

    assert(net_fd >= 0);

    len = sizeof(sin);
    while ((r = recvfrom(net_fd, buf, MAX_PAYLOAD, 0, (struct sockaddr *)&sin,
                    &len)) != 0) {
        if (r < 0) {
            if (errno == EINTR) {
                debug(INFO, "recvfrom system call interrupted");
                continue;
            }
            else if (errno == EAGAIN || errno == EWOULDBLOCK)
                return 1;
            else {
                debug(INFO, "recvfrom: %s", strerror(errno));
                return 1;
            }
        }

        if (!query_new(&sin, buf, r))
            return 0;
    }

    return 1;
}

static void _net_dump(void) {
    struct llhead *lp, *tmp;
    sendq_t *q;

    assert(net_fd >= 0);

    LL_FOREACH_SAFE(&sendq_list_head, lp, tmp) {
        q = LL_ENTRY(lp, sendq_t, sendq_list);
        while (sendto(net_fd, q->buf, q->len, 0, (struct sockaddr *)&q->sin,
                    sizeof(q->sin)) < 0)
            if (errno == EINTR) {
                debug(INFO, "sendto system call interrupted");
                continue;
            }
            else if (errno == EAGAIN || errno == EWOULDBLOCK)
                return;
            else {
                debug(INFO, "sendto: %s", strerror(errno));
                return;
            }

        --sendq_count;
        debug(WARN, "sent a query to %s", inet_ntoa(q->sin.sin_addr));
        LL_DEL(lp);
        free(q);
    }
}

static int _net_sendq_add(struct sockaddr_in *sin, char *buf, size_t len) {
    sendq_t *q;

    assert(sin != NULL);
    assert(buf != NULL);

    if ((q = malloc(sizeof(*q) + len)) == NULL) {
        err(1, "malloc");
        return 0;
    }

    q->buf = (char *)q + sizeof(*q);
    q->len = len;
    memcpy(q->buf, buf, len);
    memcpy(&q->sin, sin, sizeof(*sin));

    LL_ADD(&sendq_list_head, &q->sendq_list);

    debug(WARN, "added a sendq of size %d", len);
    return 1;
}

void net_loop(int fd) {
    struct pollfd pfd;

    net_fd = fd;

    pfd.fd = fd;
    pfd.events = POLLIN;
    if (sendq_count > 0)
        pfd.events |= POLLOUT;

    while (exit_code == 0) {
        if (poll(&pfd, 1, 1000) < 0) {
            if (errno == EINTR) {
                debug(INFO, "poll system call interrupted");
                continue;
            }
            else {
                err(1, "poll");
                return;
            }
        }

        /* see if we need to update the default resolver */
        if (update_resolver) {
            if (!query_default_resolver()) {
                /* an error message is printed by query_default_resolver */
                exit_code = 1;
                return;
            }
            update_resolver = 0;
        }

        /* first handle the errors */
        if (pfd.revents & POLLERR) {
            debug(CRIT, "error on socket");
            return;
        }
        if (pfd.revents & POLLHUP) {
            debug(CRIT, "socket hang up");
            return;
        }
        if (pfd.revents & POLLNVAL) {
            debug(CRIT, "socket not open");
            return;
        }

        if (pfd.revents & POLLIN) {
            if (!_net_recv())
                return;
        }
        if (pfd.revents & POLLOUT)
            _net_dump();

        /* only wait for pollout if we've something to send */
        if (sendq_count > 0)
            pfd.events |= POLLOUT;
        else
            pfd.events &= ~POLLOUT;
    }
}

/* the only tiem this should ever fail is memory can't be allocated */
int net_send(struct sockaddr_in *sin, char *buf, size_t len) {
    assert(buf != NULL);
    assert(net_fd >= 0);

    if (sendq_count > 0)
        return _net_sendq_add(sin, buf, len);
    else
        while (sendto(net_fd, buf, len, 0, (struct sockaddr *)sin,
                    sizeof(*sin)) < 0)
            if (errno == EINTR) {
                debug(INFO, "interrupted sendto");
                continue;
            }
            else if (errno == EAGAIN || errno == EWOULDBLOCK)
                return _net_sendq_add(sin, buf, len);
            else {
                debug(INFO, "sendto: %s", strerror(errno));
                return 1;
            }

    return 1;
}

int _net_socket(int family) {
    int fd;
    long flags;

    if ((fd = socket(PF_INET, family, 0)) < 0) {
        err(1, "socket");
        return -1;
    }

    if ((flags = fcntl(fd, F_GETFL)) < 0
            || fcntl(fd, F_SETFL, flags | O_NONBLOCK) < 0) {
        err(1, "fcntl");
        return -1;
    }

    return fd;
}

int net_socket_bind(int family, struct in_addr addr, unsigned short port) {
    int fd, reuse;
    struct sockaddr_in sin;

    if ((fd = _net_socket(family)) < 0)
        return -1;

    sin.sin_family = AF_INET;
    sin.sin_addr = addr;
    sin.sin_port = htons(port);

    /* make sure we can reuse the socket */
    reuse = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0) {
        err(1, "setsockopt");
        close(fd);
        return -1;
    }

    while (bind(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
        if (errno != EINTR) {
            debug(CRIT, "bind: %s", strerror(errno));
            close(fd);
            return -1;
        }

    return fd;
}

int net_socket_connect(int family, struct in_addr addr, unsigned short port) {
    int fd;
    struct sockaddr_in sin;

    if ((fd = _net_socket(family)) < 0)
        return -1;

    sin.sin_family = AF_INET;
    sin.sin_addr = addr;
    sin.sin_port = htons(port);

    /* if it connects immediately, the logic is the same */
    while (connect(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
        if (errno == EINPROGRESS)
            return fd;
        else if (errno != EINTR) {
            debug(INFO, "connect: %s", strerror(errno));
            close(fd);
            return -1;
        }

    return fd;
}

void net_cleanup(void) {
    struct llhead *lp, *tmp;
    sendq_t *q;

    LL_FOREACH_SAFE(&sendq_list_head, lp, tmp) {
        q = LL_ENTRY(lp, sendq_t, sendq_list);
        debug(INFO, "free'd a sendq");
        LL_DEL(lp);
        free(q);
    }
}
