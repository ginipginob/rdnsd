#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h> /* for strerror */
#include <sys/poll.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "rdnsd.h"

extern int exit_code;
extern int tcp_max_conn;

/* tcp_query_destroy will modify this */
int query_avail;

#define N_FDS (2 * tcp_max_conn + 1)

typedef struct _event_cb_t {
    query_cb_t cb;
    query_err_cb_t err;
    tcp_query_t *query;
} event_cb_t;

static struct pollfd *pfd;
static event_cb_t *cb;

/* returns an entry in the table */
fd_entry_t tcp_net_new_fd(int fd, tcp_query_t *q, query_err_cb_t err_cb) {
    int i;

    for (i = 1; i < N_FDS; ++i)
        if (pfd[i].fd < 0) {
            /* add it to the poll fds */
            pfd[i].fd = fd;
            pfd[i].events = pfd[i].revents = 0;

            /* add the callbacks */
            cb[i].err = err_cb;
            cb[i].query = q;

            debug(NOIS, "new fdi %d for fd %d", i, fd);
            return i;
        }

    /* should never get here */
    debug(CRIT, "no fd entries available (%d quer%s available)", query_avail,
            query_avail == 1 ? "y" : "ies");
    exit_code = 1;
    return -1;
}

/* listen for read and call the callback */
void tcp_net_read_cb(fd_entry_t i, query_cb_t rcb) {
    assert(pfd[i].fd >= 0);
    pfd[i].events = POLLIN;
    cb[i].cb = rcb;
}

/* listen for write and call the callback when available */
void tcp_net_write_cb(fd_entry_t i, query_cb_t wcb) {
    assert(pfd[i].fd >= 0);
    pfd[i].events = POLLOUT;
    cb[i].cb = wcb;
}

/* clears the callback, fd will only react to errors */
void tcp_net_clear_cb(fd_entry_t i) {
    assert(pfd[i].fd >= 0);
    pfd[i].events = 0;
}

/* gets rid of an entry in the table, returns revents for kicks */
int tcp_net_remove_fd(fd_entry_t i) {
    pfd[i].fd = -1;
    return pfd[i].revents;
}

/* accept a new connection
   returns:
   -1 on critical error
   number of active file descriptors removed otherwise (can be zero) */
static int _tcp_net_accept(int in_fd, tcp_query_t *nu) {
    int fd, count;
    struct sockaddr_in sin;
    socklen_t len;

    /* number of file descriptors by which we should reduce count */
    count = 0;

    len = sizeof(sin);
    for ( ; ; ) {
        if ((fd = accept(in_fd, (struct sockaddr *)&sin, &len)) < 0) {
            if (errno == EINTR) {
                debug(INFO, "accept system call interrupted");
                continue;
            }
            else if (errno == EAGAIN || errno == EWOULDBLOCK)
                return count;
            else {
                debug(CRIT, "accept failed: %s", strerror(errno));
                return 0;
            }
        }

        debug(WARN, "accepted connection from %s", inet_ntoa(sin.sin_addr));

        /* create the query, dropping the LRU if necessary */
        tcp_query_new(fd, &sin);
    }
}

/* abort when there's an error on the main socket */
static int _tcp_net_abort(int in_fd, int err, tcp_query_t *nu) {
    if (err & POLLERR)
        debug(CRIT, "error condition on TCP socket");
    if (err & POLLHUP)
        debug(CRIT, "TCP socket hung up");
    if (err & POLLNVAL)
        debug(CRIT, "TCP socket not open");

    debug(CRIT, "exiting due to error on TCP socket");
    exit_code = 1;

    return 0;
}

/* main loop for TCP connections */
void *tcp_net_loop(void *p) {
    int i, fd;

    assert(p != NULL);

    fd = *(int *)p;
    query_avail = tcp_max_conn;

    /* allocate all the pollfd structs */
    if ((pfd = malloc(sizeof(*pfd) * N_FDS)) == NULL) {
        err(1, "malloc");
        return NULL;
    }

    /* set the default values */
    for (i = 1; i < tcp_max_conn * 2 + 1; ++i) {
        pfd[i].fd = -1;
        pfd[i].events = 0;
    }

    /* listen for new connections on the main socket */
    pfd[0].fd = fd;
    pfd[0].events = POLLIN;

    /* allocate all the callback structures */
    if ((cb = malloc(sizeof(*cb) * N_FDS)) == NULL) {
        err(1, "malloc");
        return NULL;
    }

    /* set the callbacks for the main accepting socket */
    cb[0].cb = _tcp_net_accept;
    cb[0].err = _tcp_net_abort;

    /* wake up once per second to make sure we don't need to exit */
    while (exit_code == 0) {
        int count, closed;

        if ((count = poll(pfd, N_FDS, 1000)) < 0) {
            if (errno == EINTR) {
                debug(INFO, "poll system call interrupted");
                continue;
            }
            else {
                err(1, "poll");
                return NULL;
            }
        }

        if (count)
            debug(NOIS, "poll returned %d", count);

        /* short circuit quit once we've touched all FDs */
        for (i = 0; count && i < N_FDS; ++i) {
            /* skip empty structures and those without events */
            if (pfd[i].fd < 0 || pfd[i].revents == 0)
                continue;

            --count;

            if (pfd[i].revents & (POLLERR | POLLHUP | POLLNVAL)) {
                if ((closed = !cb[i].err(pfd[i].fd, pfd[i].revents,
                                cb[i].query)) != 0)
                    return NULL;

                /* if there was an error, don't bother reading/writing */
                count -= closed;
                pfd[i].revents = 0;
                continue;
            }

            closed = 0;
            if (pfd[i].revents & (POLLIN | POLLOUT)) {
                /* debug(NOIS, "[%d] calling %p with fd %d", i, cb[i].query,
                        pfd[i].fd); */
                if ((closed = cb[i].cb(pfd[i].fd, cb[i].query)) < 0)
                    return NULL;
            }
            count -= closed; /* this is getting to be a pain */

            pfd[i].revents = 0;
        }
    }

    tcp_net_cleanup();
    tcp_query_cleanup();

    return NULL;
}

/* clean up memory */
void tcp_net_cleanup(void) {
    free(pfd);
    free(cb);
}
