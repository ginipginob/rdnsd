#!/bin/sh

# grab the email address if given
if [ "$1" = "-e" ]; then
    MAIL=$2
    shift 2
fi

# die if there aren't any commands
if [ "" = "$1" ]; then
    echo no command given
    exit 1
fi

# restart the daemon as necessary
while true; do
    $@ -f;
    if [ "" != "$MAIL" ]; then
        echo failure at `date -u` | mailx -s 'rdnsD failure' $MAIL
    fi
    sleep 1;
done
