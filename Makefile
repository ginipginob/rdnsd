PROG = rdnsD
RPMVER = 0.0.2
RPMREV = 1
RPMSNAM = $(PROG)-$(RPMVER)
RPMLNAM = $(PROG)-$(RPMVER)-$(RPMREV)
OBJS = b64.o main.o md5.o net.o proto.o query.o tcp_net.o tcp_query.o

CFLAGS += -Wall -Werror -D_REENTRANT
CFLAGS += -DDEBUG -g
# CFLAGS += -DNDEBUG -O2
# CFLAGS += -D_ALWAYS_TRUNC

LDFLAGS = -lpthread

.PHONY: all
all: $(PROG)

.PHONY: install
install: $(PROG)
	for i in usr usr/bin etc etc/$(PROG) etc/init.d; do\
	  if [ ! -d $(DESTDIR)/$$i ]; then\
	    mkdir -p $(DESTDIR)/$$i;\
	    chmod 755 $(DESTDIR)/$$i;\
	  fi;\
	done
	cp $(PROG) $(DESTDIR)/usr/bin/$(PROG)
	chmod 755 $(DESTDIR)/usr/bin/$(PROG)
	cp $(PROG).conf.sample $(DESTDIR)/etc/$(PROG)/$(PROG).conf
	chmod 644 $(DESTDIR)/etc/$(PROG)/$(PROG).conf
	cp $(PROG)ctl $(DESTDIR)/etc/init.d/$(PROG)
	chmod 755 $(DESTDIR)/etc/init.d/$(PROG)

.PHONY: rpm
rpm: $(RPMLNAM).el6.x86_64.rpm

$(RPMLNAM).el6.x86_64.rpm:
	mkdir -p $${HOME}/rpmbuild/SOURCES
	mkdir -p $${HOME}/rpmbuild/SPECS
	cp rpmspec/$(PROG).spec $${HOME}/rpmbuild/SPECS
	tar --transform 's,^,$(RPMSNAM)/,' -c -z -f $${HOME}/rpmbuild/SOURCES/$(PROG).tar.gz Makefile *.h *.c $(PROG).conf.sample $(PROG)ctl
	cd $${HOME}/rpmbuild/SPECS; rpmbuild -bb --define='RPMVER $(RPMVER)' --define='RPMREV $(RPMREV)' $(PROG).spec
	cp $$(find $${HOME}/rpmbuild/RPMS -name '$(RPMLNAM)*.rpm') $@
	rm -rf $${HOME}/rpmbuild

$(PROG): $(OBJS)
	$(CC) -o $(PROG) $(LDFLAGS) $(OBJS)

clean:
	rm -f $(PROG) $(OBJS)
