#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin";
use Getopt::Long;
use AnsibleSiteFile;
use Data::Dumper;

my %groupnames = (
    hongkong => 'Hong Kong',
    losangeles => 'Los Angeles',
    newyork => 'New York',
    sanfrancisco => 'San Francisco'
);
sub displayGroupName {
    my($groupname) = @_;

    exists($groupnames{$groupname}) ? $groupnames{$groupname} : ucfirst($groupname);
}

sub bySiteTable {
    my($groups) = @_;

    print "<h3>By Site</h3>\n";
    print "<table>\n";
    print " <thead>\n";
    print "  <tr><th>Site</th><th>Net Block</th><th>Host</th><th>IPv4</th><th>Port</th><th>Service</th></tr>\n";
    print " </thead>\n";
    print " <tbody>\n";
    for my $groupname (sort($groups->getGroupNames())) {
        next if ($groups->getGroupVar($groupname, 'description'));
        my $group = $groups->getGroup($groupname);
        my $dgroupname = displayGroupName($groupname);
        my $netblock = $groups->getGroupVar($groupname, 'edge_subnet');
        my @rows = ();

        for my $hostname (sort(keys(%{$group->{hosts}}))) {
            my $host = $group->{hosts}->{$hostname};
            my $hostaddr = $groups->getHostVar($host, 'edge_addr');

            for my $servicename (sort(@{$host->{groupnames}})) {
                my $description = $groups->getGroupVar($servicename, 'description');
                next unless ($description);
                my $ipv4 = $hostaddr ? $netblock.'.'.$hostaddr : '?';
                my $port = $groups->getHostVar($host, $servicename.'_port');
                my $serviceDescription = $groups->getHostVar($host, $servicename.'_service');
                $description .= "<br/>$serviceDescription" if ($serviceDescription);
                push(@rows, "<td>$hostname</td><td>$ipv4</td><td>$port</td><td>$description</td>");
            }
        }
        my $rowCount = @rows;
        if ($rowCount) {
            if ($rowCount > 1) {
                print "  <tr><td rowspan='$rowCount'>$dgroupname</td><td rowspan='$rowCount'>$netblock</td>", $rows[0], "</tr>\n";
                for (my $rowI = 1; $rowI < $rowCount; $rowI += 1) {
                    print "  <tr>", $rows[$rowI], "</tr>\n";
                }
            } else {
                print "  <tr><td>$dgroupname</td><td>$netblock</td>", $rows[0], "</tr>\n";
            }
        } else {
            print "  <tr><td>$dgroupname</td><td>$netblock</td><td colspan='4'></td></tr>\n";
        }
    }
    print " </tbody>\n";
    print "</table>\n";
}

sub byServiceTable {
    my($groups) = @_;

    print "<h3>By Service</h3>\n";
    print "<table>\n";
    print " <thead>\n";
    print "  <tr><th>Service</th><th>Host</th><th>IP</th><th>Port</th><th>Component</th></tr>\n";
    print " </thead>\n";
    print " <tbody>\n";
    for my $servicename (sort($groups->getGroupNames())) {
        my $description = $groups->getGroupVar($servicename, 'description');
        next unless ($description);
        my $service = $groups->getGroup($servicename);
        my @rows = ();

        for my $hostname (sort(keys(%{$service->{hosts}}))) {
            my $host = $service->{hosts}->{$hostname};
            my $netblock = $groups->getHostVar($host, 'edge_subnet');
            my $hostaddr = $groups->getHostVar($host, 'edge_addr');
            my $ipv4 = $netblock && $hostaddr ? $netblock.'.'.$hostaddr : '?';
            my $port = $groups->getHostVar($host, $servicename.'_port');
            my $component = $groups->getHostVar($host, $servicename.'_component');
            $component = '' unless ($component);
            push(@rows, "<td>$hostname</td><td>$ipv4</td><td>$port</td><td>$component</td>");
        }

        my $rowCount = @rows;
        if ($rowCount) {
            if ($rowCount > 1) {
                print "  <tr><td rowspan='$rowCount'>$description</td>", $rows[0], "</tr>\n";
                for (my $rowI = 1; $rowI < $rowCount; $rowI += 1) {
                    print "  <tr>", $rows[$rowI], "</tr>\n";
                }
            } else {
                print "  <tr><td>$description</td>", $rows[0], "</tr>\n";
            }
        } else {
            print "  <tr><td>$description</td><td colspan='4'></td></tr>\n";
        }
    }
    print " </tbody>\n";
    print "</table>\n";
}

sub main {
    my($sitesFilename) = @_;
    my $groups = AnsibleSiteFile->new($sitesFilename);

    bySiteTable($groups);
    byServiceTable($groups);
}


my %options = (sites => 'sites/cto-edge');
GetOptions(\%options, 'sites=s')
    || die("Failed to parse options.");

main($options{sites});
