
package AnsibleSiteFile;

use strict;
use warnings;
use base qw(Exporter);
@AnsibleSiteFile::EXPORT = qw(getGroupNames getGroup getHostVar getGroupVar new);

sub getGroupNames {
    my($self) = @_;

    keys %$self;
}

sub getGroup {
    my($self, $groupname) = @_;

    exists($self->{$groupname}) ? $self->{$groupname} : undef;
}

sub getHostVar {
    my($self, $host, $varname) = @_;
    my $value;

    if (exists($host->{vars}->{$varname})) {
        $value = $host->{vars}->{$varname};
    } else {
        for my $groupname (@{$host->{groupnames}}) {
            $value = $self->getVarFromGroup($groupname, $varname, {});
            last if (defined($value));
        }
    }

    $value;
}

sub getGroupVar {
    my($self, $groupname, $varname) = @_;
    my $value;

    $self->getVarFromGroup($groupname, $varname, {});
}

sub getVarFromGroup {
    my($self, $groupname, $varname, $visited) = @_;
    my $value;

    unless ($visited->{$groupname}) {
        $visited->{$groupname} = 1;

        if (exists($self->{$groupname})) {
            my $group = $self->{$groupname};

            if (exists($group->{vars}->{$varname})) {
                $value = $group->{vars}->{$varname};
            } else {
                for my $groupname (@{$group->{groupnames}}) {
                    $value = $self->getVarFromGroup($groupname, $varname, $visited);
                    last if (defined($value));
                }
            }
        }
    }

    $value;
}

sub loadVars {
    my($host, $varsval) = @_;
    my $count = 0;

    while ($varsval =~ /(.*?)=(\S*)\s*/g) {
        $host->{vars}->{$1} = $2;
        $count++;
    }

    $count;
}

sub findHost {
    my($hosts, $hostname) = @_;
    my $host = $hosts->{$hostname};

    unless (defined($host)) {
        $host = { hostname => $hostname, groupnames => [], vars => {} };
        $hosts->{$hostname} = $host;
    }

    $host;
}

sub findGroup {
    my($self, $groupname) = @_;
    my $group = $self->{$groupname};

    unless (defined($group)) {
        $group = { groupname => $groupname, groupnames => [], hosts => {}, vars => {} };
        $self->{$groupname} = $group;
    }

    $group;
}

sub new {
    my $class = shift;
    my $siteFilename = shift;
    my $hosts = {};
    my $self = bless({}, $class);
    my($NIL, $VARS, $CHILDREN, $GROUP, $HOST) = (0, 1, 2, 3);
    my $state = $NIL;
    my $group;

    open(my $f, '<', $siteFilename) || die("Cannot open $siteFilename: $!\n");
    while (<$f>) {
        if (/\[(.*):vars\]/) {
            $group = $self->findGroup($1);
            $state = $VARS;
        } elsif (/\[(.*):children\]/) {
            $group = $self->findGroup($1);
            $state = $CHILDREN;
        } elsif (/\[(.*)\]/) {
            $group = $self->findGroup($1);
            $state = $GROUP;
        } elsif ($state == $VARS && /(.+?)=(.*)/) {
            die("Variable definition found but group not set: $_") unless (defined($group));
            $group->{vars}->{$1} = $2;
        } elsif ($state == $CHILDREN && /(\S+)/) {
            die("Child group found but group not set: $_") unless (defined($group));
            my $childGroup = $self->findGroup($1);
            push(@{$group->{groupnames}}, $childGroup->{groupname});
        } elsif ($state == $GROUP && /(\S+)\s+(.+)/) {
            die("Host with variables found but group not set: $_") unless (defined($group));
            my $host = findHost($hosts, $1);
            push(@{$host->{groupnames}}, $group->{groupname});
            loadVars($host, $2) || die("Cannot load host vars from: $_");
            $group->{hosts}->{$host->{hostname}} = $host;
        } elsif ($state == $GROUP && /(\S+)/) {
            die("Host found but group not set: $_") unless (defined($group));
            my $host = findHost($hosts, $1);
            push(@{$host->{groupnames}}, $group->{groupname});
            $group->{hosts}->{$host->{hostname}} = $host;
        } else {
            die("Cannot grok line: $_") unless (/^\s*$/);
        }
    }
    close($f);

    $self;
}

1;
