#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin";
use Getopt::Long;
use AnsibleSiteFile;

sub getHostAddress {
    my($groups, $host) = @_;

    $groups->getHostVar($host, 'edge_subnet').'.'.$groups->getHostVar($host, 'edge_addr');
}

sub main {
    my($options) = @_;
    my $groups = AnsibleSiteFile->new($options->{sites});
    my $qname = $options->{qname};
    my $tsig = $options->{tsig};

    $tsig = 'rdnsd-key:'.$tsig unless ($tsig =~ /:/);

    my $secspider = $groups->getGroup('secspider');
    for my $hostname (sort keys %{$secspider->{hosts}}) {
        my $host = $secspider->{hosts}->{$hostname};
        my $hostaddr = getHostAddress($groups, $host);
        my $cmd = "dig -y $tsig +time=2 +tries=2 \@$hostaddr $qname";
        my @lines = `$cmd`;
        my %info = (answers => []);

        foreach (@lines) {
            if (/;; connection timed out; no servers could be reached/) {
                $info{status} = 'TIMEOUT';
            }
            if (/;; flags: .*?; QUERY: 1, ANSWER: (\d+), AUTHORITY: \d+, ADDITIONAL: \d+/) {
                $info{status} = $1 == 1 ? "OK $1 answer" : "OK $1 answers";
            }
            if (/;; Query time: (\d+) msec/) {
                $info{qtime} = $1;
            }
            if (/^$qname\.\s+\d+\s+IN\s+A\s+([0-9.]+)/) {
                push(@{$info{answers}}, $1);
            }
        }
        if (exists($info{status})) {
            my $hostname = $host->{hostname};
            my $status = $info{status};
            my $answers = ' '.join(' ', @{$info{answers}});
            my $qtime = exists($info{qtime}) ? ' in '.$info{qtime}.' msec' : '';
            print "$cmd # " if ($options->{verbose});
            print "$hostname/$hostaddr: $status$answers$qtime\n";
        } else {
            print "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n";
            print "$cmd # ", $host->{hostname}, "/$hostaddr\n", @lines;
        }
    }
}

my %options = (qname => 'google.com', sites => 'sites/cto-edge', verbose => 0);
GetOptions(\%options, 'qname=s', 'sites=s', 'tsig=s', 'verbose!')
    || die("Failed to parse options.");
die("You must specify the tsig\n") unless (exists($options{tsig}));

main(\%options);
