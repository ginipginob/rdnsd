#ifndef __RDNSD_H_
#define __RDNSD_H_

#include <netinet/in.h>

/* don't use the system's err messages */
void err(int eval, char *fmt, ...);

/* constants */
#define MD5_DIGEST 16

/* configuration numbers */
#define DEFAULT_PENDING 800
#define DEFAULT_TCP 100
#define MAX_PAYLOAD 8192
#define MAX_NAMELEN 512
#define MAX_KEYLEN 512
#define _MAX_FILE_NAME 1024

/* query stuff */
int query_expire(void);
int query_new(struct sockaddr_in *sin, char *buf, size_t len);
int query_default_resolver(void);
void query_cleanup(void);

/* net utils */
void net_loop(int fd);
int net_send(struct sockaddr_in *sin, char *buf, size_t len);
int net_socket_bind(int family, struct in_addr addr, unsigned short port);
int net_socket_connect(int family, struct in_addr addr, unsigned short port);
void net_cleanup(void);

/* tcp query */
typedef struct _tcp_query_t tcp_query_t;
typedef int (*query_cb_t)(int, tcp_query_t *);
typedef int (*query_err_cb_t)(int, int, tcp_query_t *);
tcp_query_t *tcp_query_lru(void);
int tcp_query_new(int, struct sockaddr_in *);
int tcp_query_destroy(tcp_query_t *);
int tcp_query_cleanup(void);

/* i tip my hat to abstraction ;) */
typedef int fd_entry_t;

/* tcp net */
void *tcp_net_loop(void *param);
fd_entry_t tcp_net_new_fd(int fd, tcp_query_t *q, query_err_cb_t err);
int tcp_net_remove_fd(fd_entry_t);
void tcp_net_read_cb(fd_entry_t, query_cb_t);
void tcp_net_write_cb(fd_entry_t, query_cb_t);
void tcp_net_clear_cb(fd_entry_t);
void tcp_net_cleanup(void);

/* needed below */
typedef struct _tsig_data_t tsig_data_t;

/* protocol parsers */
int proto_read_name(char *buf, int len, int offset, char *nbuf, int buflen);
int proto_opt_tsig(char *buf, size_t len, int *opt, int *tsig);
int proto_tsig_verify_strip(char *buf, size_t *len, int tsig, tsig_data_t **);
int proto_opt_parse(char *buf, size_t *len, int opt, struct in_addr *dst);
int proto_tsig_add(char *buf, size_t *len, tsig_data_t *data);
int proto_tsig_size(tsig_data_t *data);

#define PROTO_STRIP 1
#define PROTO_LEAVE 2

/* debug */
#define NOIS 0
#define WARN 1
#define INFO 2
#define CRIT 3
#define NONE 4

void debug(int level, char *fmt, ...);

/* common data structures */
typedef struct _dnshead_t {
    unsigned short id;
    unsigned short flags;
    /*
    unsigned qr :1;
    unsigned opcode :4;
    unsigned aa :1;
    unsigned tc :1;
    unsigned rd :1;
    unsigned ra :1;
    unsigned z :3;
    unsigned rcode :4;
    */
    unsigned short qdcount, ancount, nscount, arcount;
} __attribute__((packed)) dnshead_t;

struct _tsig_data_t {
    char *name;
    int namelen;
    char *algo;
    int algolen;
    unsigned time;
    unsigned short fudge;

    /* normally these are predefined */
    unsigned short class;
    unsigned ttl;

    /* this is for recalculating the TSIG on the response */
    char *old_digest;
};

typedef struct rdnsd_log_ctx_s
{
  char *m_szFile;
  size_t m_uMaxSize;
  int m_iMaxBakFiles;
} rdsnd_log_ctx_t;

#endif /* __RDNSD_H__ */
