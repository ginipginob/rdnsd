#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use File::Spec;

sub read_file {
    my($filename) = @_;
    my @r = ();

    if (open(my $f, '<', $filename)) {
        @r = <$f>;
        close($f);
    }

    \@r;
}

sub main {
    my($src, $dst) = @_;

    open(my $i, '<', $src) || die("Cannot read $src: $!\n");
    my @tmpl = <$i>;
    close($i);

    open(my $o, '>', $dst) || die("Cannot write $dst: $!\n");
    my %vars = ();
    $vars{SSH_PVT_KEY_GIT} = read_file(File::Spec->catfile($ENV{HOME}, '.ssh', 'id_rsa'));
    $vars{SSH_PUB_KEY_GIT} = read_file(File::Spec->catfile($ENV{HOME}, '.ssh', 'id_rsa.pub'));
    $vars{SSH_PVT_KEY_PROD} = read_file(File::Spec->catfile($ENV{HOME}, '.ssh', 'id_prod'));
    $vars{SSH_PUB_KEY_PROD} = read_file(File::Spec->catfile($ENV{HOME}, '.ssh', 'id_prod.pub'));
    for my $line (@tmpl) {
        if ($line =~ "###(.*)###" && exists($vars{$1})) {
            print $o @{$vars{$1}};
        } else {
            print $o $line;
        }
    }
    close($o);
}

my %options = (src => 'bootstrap.sh.tmpl', dst => 'bootstrap.sh');

GetOptions(\%options, 'src=s', 'dst=s')
    || die("Failed to parse options.");

main($options{src}, $options{dst});
