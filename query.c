#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "rdnsd.h"
#include "llist.h"

#define ADDR_EQUAL(X, Y) (*(unsigned *)&(X) == *(unsigned *)&(Y))

typedef struct _query_t {
    struct in_addr dst;
    struct sockaddr_in src;
    unsigned short id;
    tsig_data_t *tsig;
    struct llhead query_list;
} query_t;

/* default resolver, as used by UDP and TCP */
struct in_addr def_res;

static LL_HEAD(query_list_head);

/* max number of outstanding queries */
extern int lru_max;
static int query_count = 0;

/* locate a query given an ID and a remote address
   if successful, return the sockaddr to relay the reply into dst */
static query_t *_query_locate(unsigned short id, struct in_addr remote) {
    query_t *q;
    struct llhead *lp, *tmp;

    LL_FOREACH_SAFE(&query_list_head, lp, tmp) {
        q = LL_ENTRY(lp, query_t, query_list);
        if (q->id == id) {
            if (ADDR_EQUAL(remote, q->dst)) {
                debug(WARN, "query %hu found", id);
                --query_count;
                LL_DEL(lp);
                return q;
            }
            else
                debug(INFO, "query found with unmatching addresses");
        }
    }

    debug(INFO, "query %hu not found", id);
    return NULL;
}

/* destroy a query */
void _query_destroy(query_t *q) {
    free(q->tsig);
    free(q);
}

/* create a query, destroying the LRU if necessary */
query_t *_query_create(void) {
    if (query_count == lru_max) {
        query_t *last;

        /* get and remove from linked list */
        last = LL_ENTRY(query_list_head.prev, query_t, query_list);
        LL_DEL(&last->query_list);

        debug(WARN, "dropping query %hu (LRU)", last->id);
        _query_destroy(last);
    }
    else
        ++query_count;

    return malloc(sizeof(query_t));
}

/* handle an incoming query and forward it on if it isn't a reply */
int query_new(struct sockaddr_in *sin, char *buf, size_t len) {
    dnshead_t *h;
    query_t *q;
    unsigned short flags;

    assert(sin != NULL);
    assert(buf != NULL);

    if (len < sizeof(*h)) {
        debug(INFO, "packet was too short: %d < %d", len, sizeof(*h));
        return 1;
    }

    h = (dnshead_t *)buf;
    flags = htons(h->flags);

    if (flags & 0x8000) { /* query is a reply */
        if ((q = _query_locate(htons(h->id), sin->sin_addr)) == NULL) {
            debug(INFO, "query %hu was not found", htons(h->id));
            return 1;
        }
        debug(WARN, "received a query response with id %hu", q->id);

        /* these can fail, but only due to malloc */
        if (!proto_tsig_add(buf, &len, q->tsig)) {
            debug(CRIT, "failed to add TSIG to packet");
            return 0;
        }
        if (!net_send(&q->src, buf, len)) {
            debug(CRIT, "failed to send packet or add it to the queue");
            return 0;
        }

        _query_destroy(q);
    }
    else { /* query needs to be reflected */
        int opt, tsig, opt_ret;
        struct sockaddr_in sout;
        struct in_addr dst;
        tsig_data_t *data;

        debug(WARN, "received a query with id %hu", htons(h->id));

        /* for a rare corner case when packet has an OPT but no resolver */
        dst.s_addr = 0;

        /* locate the OPT and TSIG */
        if (!proto_opt_tsig(buf, len, &opt, &tsig)) {
            debug(WARN, "packet has no OPT or TSIG record");
            return 1;
        }

        /* give up on packet with no TSIG */
        if (tsig < 0) {
            debug(WARN, "received packet without TSIG from %s",
                    inet_ntoa(sin->sin_addr));
            return 1;
        }

        /* exit on allocation failure */
        if (!proto_tsig_verify_strip(buf, &len, tsig, &data)) {
            debug(CRIT, "couldn't verify TSIG");
            return 0;
        }

        /* abort if there's an invaild tsig */
        if (data == NULL) {
            debug(CRIT, "received UDP packet with counts (qd %d, an %d, ns %d, ar %d) with invalid TSIG from %s",
                  h->qdcount,
                  h->ancount,
                  h->nscount,
                  h->arcount,
                  inet_ntoa(sin->sin_addr));
            return 1;
        }

        if (!(opt_ret = proto_opt_parse(buf, &len, opt, &dst))) {
            debug(WARN, "error or kill in OPT from %s",
                    inet_ntoa(sin->sin_addr));
            return 1;
        }

        if (opt_ret == PROTO_STRIP)
            h->arcount = htons(ntohs(h->arcount) - 1);

        if ((q = _query_create()) == NULL) {
            err(1, "malloc");
            return 0;
        }

        /* default resolver */
        if (!dst.s_addr)
            q->dst = def_res;
        else /* resolver given in packet */
            q->dst = dst;

        debug(NOIS, "remote resolver is %s", inet_ntoa(q->dst));

        /* add the query to the list */
        memcpy(&q->src, sin, sizeof(*sin));
        q->id = htons(h->id);
        q->tsig = data;
        LL_ADD(&query_list_head, &q->query_list);

        sout.sin_family = AF_INET;
        sout.sin_port = htons(53);
        sout.sin_addr = q->dst;
        if (!net_send(&sout, buf, len))
            return 0;
    }

    return 1;
}

int query_default_resolver(void) {
    FILE *f;
    char buf[BUFSIZ];
    unsigned a, b, c, d;

    if ((f = fopen("/etc/resolv.conf", "r")) == NULL) {
        debug(CRIT, "cannot open resolv.conf");
        return 0;
    }

    while (fgets(buf, BUFSIZ, f) != NULL)
        if (sscanf(buf, "nameserver %u.%u.%u.%u", &a, &b, &c, &d) == 4) {
            def_res.s_addr = htonl((unsigned)a << 24 | b << 16 | c << 8 | d);
            fclose(f);
            debug(INFO, "default resolver is %s", inet_ntoa(def_res));
            return 1;
        }

    fclose(f);
    debug(CRIT, "couldn't find nameserver line in resolv.conf");
    return 0;
}

void query_cleanup(void) {
    query_t *q;
    struct llhead *lp, *tmp;

    LL_FOREACH_SAFE(&query_list_head, lp, tmp) {
        q = LL_ENTRY(lp, query_t, query_list);
        LL_DEL(lp);
        debug(INFO, "destroying query %hu", q->id);
        _query_destroy(q);
    }
}
