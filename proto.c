#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "rdnsd.h"
#include "md5.h"
#include "b64.h"

#define TYPE_OPT 41
#define TYPE_TSIG 250

#define OP_RSERVER 0x50
#define OP_RSERVER_STRIP 0x51
#define OP_KILL 0x52

extern unsigned char key[MAX_KEYLEN];
extern int keylen;
extern int exit_code;

/* returns 1 if skipped or 0 if not enough bytes left */
static int _proto_skip_name(char *buf, int len, int *offset) {
    unsigned char nlen, *b;

    b = (unsigned char *)buf;
    for ( ; ; ) {
        if (*offset >= len)
            return 0;

        nlen = b[*offset];
        ++(*offset);

        /* end of name -or- compression pointer: end of this name in packet */
        if (nlen == 0)
            return 1;
        if (nlen > 63) {
            ++(*offset); /* for the second byte of the compression */
            return 1;
        }

        *offset += nlen;
    }
}

int proto_read_name(char *buf, int len, int offset, char *nbuf, int buflen) {
    unsigned char *b, nlen;
    int tlen;

    b = (unsigned char *)buf;
    tlen = 0;

    for ( ; ; ) {
        if (offset >= len || buflen <= 0)
            return 0;

        nlen = b[offset++];
        ++tlen;

        if (nlen == 0) {
            *nbuf = 0;
            return tlen;
        }

        if (nlen > 63) {
            int addl_len;

            /* need to get one more byte for the length */
            if (offset == len)
                return 0;

            offset = ((nlen & 63) << 8) + buf[offset];
            /* recursively get the rest of the name */
            if (nlen < 192 || !(addl_len = proto_read_name(buf, len, offset,
                            nbuf, buflen)))
                return 0;

            /* tlen gets incremented by 1 for one of the pointer bytes */
            return tlen - 1 + addl_len;
        }

        *nbuf++ = nlen;
        --buflen;
        if (buflen < nlen)
            return 0;
        tlen += nlen;

        for ( ; nlen > 0; --nlen, ++offset)
            *nbuf++ = tolower(buf[offset]);
    }

    /* normal return occurs inside the for loop above */
    debug(CRIT, "shouldn't get here in DNS name extraction");
    exit_code = 1;

    return 0;
}

/* returns type if skipped or 0 if not enough bytes left */
static int _proto_skip_rr(char *buf, int len, int *offset) {
    unsigned short rdlen;
    unsigned short type;

    if (!_proto_skip_name(buf, len, offset))
        return 0;

    if (*offset + 10 > len) /* not enough bytes for the rdlen */
        return 0;

    rdlen = ntohs(*(unsigned short *)(buf + *offset + 8));
    if (*offset + 10 + rdlen > len)
        return 0;

    /* get the type and skip the rest of the RR */
    type = ntohs(*(unsigned short *)(buf + *offset));
    *offset += 10 + rdlen;

    return type;
}

int proto_opt_tsig(char *buf, size_t len, int *opt, int *tsig) {
    dnshead_t *h;
    int i, offset;

    h = (dnshead_t *)buf;

    *opt = -1;
    *tsig = -1;

    if (h->arcount == 0) /* same regardless of byte order */
        return 0;

    offset = sizeof(*h); /* skip the header */

    /* questions are just different */
    for (i = ntohs(h->qdcount); i > 0; --i) {
        if (!_proto_skip_name(buf, len, &offset) || offset + 4 > len)
            goto too_short;
        offset += 4;
        debug(NOIS, "skipped an rr, now at offset 0x%02x", offset);
    }

    for (i = ntohs(h->ancount) + ntohs(h->nscount); i > 0; --i) {
        if (!_proto_skip_rr(buf, len, &offset))
            goto too_short;
        debug(NOIS, "skipped an rr, now at offset 0x%02x", offset);
    }

    /* get the TSIG and OPT records */
    for (i = ntohs(h->arcount); i > 0; --i) {
        int type, oldoffset;
        oldoffset = offset;

        if (!(type = _proto_skip_rr(buf, len, &offset)))
            goto too_short;

        if (type == TYPE_OPT) {
            debug(NOIS, "found OPT at offset 0x%02x", oldoffset);
            *opt = oldoffset;
        }
        else if (type == TYPE_TSIG) {
            debug(NOIS, "found TSIG at offset 0x%02x", oldoffset);
            *tsig = oldoffset;
        }
    }

    if (offset < len) {
        debug(INFO, "packet had spurious bytes: %d < %d", offset, len);
        return 0;
    }

    return 1;

too_short:
    debug(INFO, "packet with id %hu is too short: %d", htons(h->id), len);
    return 0;
}

/* gets the size of the TSIG header that will be used to calculate */
static int _proto_tsig_calc_size(tsig_data_t *data) {
    /* namelen      length of the key name
       algolen      length of the algo name
       18           length of miscellaneous headers
       (last part)  length of original digest, if applicable */

    return data->namelen + data->algolen + 18
        + (data->old_digest != NULL ? MD5_DIGEST + 2 : 0);
}

/* gets the size of the TSIG header that will be added to the packet */
int proto_tsig_size(tsig_data_t *data) {
    /* namelen      length of key name
       6            length of rest of DNS header
       algolen      length of the algo name
       18           length of rest of TSIG part
       (last part)  length of digest + digest len */

    return data->namelen + 6 + data->algolen + 18 + MD5_DIGEST + 2;
}

/* this is what you need to know to calculate a TSIG
sz source       value            meaning

x  TSIG RR      NAME             Key name, in canonical wire format
 2 TSIG RR      CLASS            (Always ANY in the current specification)
 4 TSIG RR      TTL              (Always 0 in the current specification)
x  TSIG RDATA   Algorithm Name   in canonical wire format
 6 TSIG RDATA   Time Signed      in network byte order
 2 TSIG RDATA   Fudge            in network byte order
 2 TSIG RDATA   Error            in network byte order
 2 TSIG RDATA   Other Len        in network byte order
 0 TSIG RDATA   Other Data       exactly as transmitted
*/
static int _proto_tsig_calc(char *buf, size_t len, tsig_data_t *data,
        unsigned char digest[MD5_DIGEST]) {
    unsigned char *cbuf;
    int tsig_len;
    tsig_len = _proto_tsig_calc_size(data);
    if ((cbuf = malloc(len + tsig_len)) == NULL) {
        err(1, "malloc");
        return 0;
    }

#define AALGO(X) (cbuf + len + data->namelen + 6 + data->algolen + X)
    /* copy the huge data structures */
    memcpy(cbuf, buf, len);
    memcpy(cbuf + len, data->name, data->namelen);
    memcpy(cbuf + len + data->namelen + 6, data->algo, data->algolen);
    if (data->old_digest != NULL) {
        *(unsigned short *)AALGO(12) = htons(MD5_DIGEST);
        memcpy(AALGO(14), data->old_digest, MD5_DIGEST);
    }

    *(unsigned short *)(cbuf + len + data->namelen) = htons(data->class);
    *(unsigned *)(cbuf + len + data->namelen + 2) = htonl(data->ttl);

    /* lame 48 bit time */
    *(unsigned short *)AALGO(0) = 0;
    *(unsigned *)AALGO(2) = htonl(data->time);

    *(unsigned short *)AALGO(6) = htons(data->fudge);

    /* kill two birds with one stone */
    *(unsigned *)AALGO(8) = 0;
#undef AALGO

    /* do the maths */
    md5_hmac(key, keylen, cbuf, len + tsig_len, digest);

    free(cbuf);
    return 1;
}

/* returns a tsig_data_t if successful, otherwise NULL */
int proto_tsig_verify_strip(char *buf, size_t *len, int tsig,
        tsig_data_t **ret) {
    dnshead_t *h;
    char name[MAX_NAMELEN], algo[MAX_NAMELEN];
    unsigned char digest[MD5_DIGEST]; /* for digest calculation */
    int name_len;
    unsigned short maclen, rdlen;
    tsig_data_t data;
    time_t now;
    unsigned *pLame = NULL;

    assert(ret != NULL);

    name_len = tsig;

    h = (dnshead_t *)buf;
    h->arcount = htons(ntohs(h->arcount) - 1); /* remove TSIG */

    /* read the name, and then skip past it */
    if (!(data.namelen = proto_read_name(buf, *len, tsig, name,
                    MAX_NAMELEN))) {
        *ret = NULL;
        debug(INFO, "couldn't read TSIG name from packet");
        return 1;
    }
    data.name = name;
    _proto_skip_name(buf, *len, &tsig);
    name_len = tsig - name_len;

    /* some useful numbers before we skip the algorithm name */
    data.class = ntohs(*(unsigned short *)(buf + tsig + 2));
    data.ttl = ntohl(*(unsigned *)(buf + tsig + 4));
    rdlen = ntohs(*(unsigned short *)(buf + tsig + 8));

    /* get the algorithm name and skip it */
    tsig += 10;
    if (!(data.algolen = proto_read_name(buf, *len, tsig, algo,
                    MAX_NAMELEN))) {
        *ret = NULL;
        debug(INFO, "couldn't read algo name from packet");
        return 1;
    }
    data.algo = algo;
    _proto_skip_name(buf, *len, &tsig);

    /* make sure the algorithm is MD5 */
    maclen = ntohs(*(unsigned short *)(buf + tsig + 8));
    if (maclen != MD5_DIGEST) {
        *ret = NULL;
        debug(INFO, "algorithm isn't MD5: maclen is %hu", maclen);
        return 1;
    }

    /* XXX ignore the whole 48 bit date thing */
    data.time = ntohl(*(unsigned *)(buf + tsig + 2));
    data.fudge = ntohs(*(unsigned short *)(buf + tsig + 6));

    now = time(NULL);
    if (data.time < now - data.fudge || data.time > now + data.fudge) {
      pLame = &data.time;
        debug(INFO, "TSIG timestamp out of date: %s",
                asctime(gmtime((time_t *) pLame)));
        *ret = NULL;
        return 1;
    }

    /* get rid of the tsig
       name_len     name length
       rdlen        data length
       10           header length */
    *len -= name_len + rdlen + 10;

    data.old_digest = NULL;
    if (!_proto_tsig_calc(buf, *len, &data, digest))
        return 0; /* memory allocation failed */

    if (memcmp(digest, buf + tsig + 10, MD5_DIGEST) != 0) {
      unsigned char pOldBuff[1024];
      unsigned char pNewBuff[1024];
      memset(pOldBuff, 0, 1024);
      memset(pNewBuff, 0, 1024);
      encode((unsigned char *) buf + tsig + 10, pOldBuff, MD5_DIGEST, 1024);
      encode(digest, pNewBuff, MD5_DIGEST, 1024);

        debug(INFO, "invalid tsig for name '%s', algo %d: '%s' != '%s'", data.name, data.algolen, pNewBuff, pOldBuff);
        *ret = NULL;
        return 1;
    }

    if ((*ret = malloc(sizeof(**ret) + data.namelen + data.algolen
                    + MD5_DIGEST)) == NULL) {
        err(1, "malloc");
        return 0;
    }
    **ret = data; /* copy all the old data out */

    (*ret)->name = (char *)*ret + sizeof(**ret);
    memcpy((*ret)->name, name, data.namelen);
    (*ret)->algo = (*ret)->name + data.namelen;
    memcpy((*ret)->algo, algo, data.algolen);
    (*ret)->old_digest = (*ret)->algo + data.algolen;
    memcpy((*ret)->old_digest, digest, MD5_DIGEST);

    return 1;
}

int proto_opt_parse(char *buf, size_t *len, int opt, struct in_addr *dst) {
    int offset, rdoffset, ret;
    unsigned short rdlen, optlen;
#ifdef _ALWAYS_TRUNC
    // For 512ifying
    char *pBufLen = NULL;
    unsigned short uBuffSize = htons(512);
#endif

    offset = opt;
    if (!_proto_skip_name(buf, *len, &offset)) {
        debug(INFO, "can't read name from OPT record");
        return 0;
    }

    ret = PROTO_LEAVE;
#ifdef _ALWAYS_TRUNC
    // Keep a pointer to the EDNS0 buff size.
    pBufLen = buf + offset + 2;
#endif
    rdlen = ntohs(*(unsigned short *)(buf + offset + 8));
    for (rdoffset = offset + 10; rdoffset < offset + 8 + rdlen;
            rdoffset += optlen + 4) {
        unsigned short opcode;

        opcode = ntohs(*(unsigned short *)(buf + rdoffset));
        optlen = ntohs(*(unsigned short *)(buf + rdoffset + 2));
        if (optlen + rdoffset + 2 > offset + 8 + rdlen) {
            debug(INFO, "optlen takes us out of the rdata");
            return 0;
        }

        if (opcode == OP_RSERVER_STRIP) {
            *len -= offset - opt + rdlen + 10;
            ret = PROTO_STRIP;
            dst->s_addr = *(unsigned *)(buf + rdoffset + 4);
        }
        else if (opcode == OP_RSERVER)
            dst->s_addr = *(unsigned *)(buf + rdoffset + 4);
        else if (opcode == OP_KILL) {
            debug(CRIT, "received remote kill");
            exit_code = 1;
            return 0;
        }
        else {
            debug(INFO, "unknown opcode: %04x\n", opcode);
            return 0;
        }
    }

#ifdef _ALWAYS_TRUNC
    // If this message is being sent to a specific
    // name server, make this a 512 packet, always
    if (0 != dst->s_addr)
    {
      memcpy(pBufLen, &uBuffSize, sizeof(uBuffSize));
    }
#endif

    if (ret == PROTO_LEAVE) {
        *(unsigned short *)(buf + offset + 8) = 0;
        *len -= rdlen;
    }

    return ret;
}

int proto_tsig_add(char *buf, size_t *len, tsig_data_t *data) {
    dnshead_t *h;
    char *tsig, *orig;
    unsigned char digest[MD5_DIGEST];

    h = (dnshead_t *)buf;
    h->arcount = htons(ntohs(h->arcount) + 1);

    /* update the time stamp */
    data->time = time(NULL);
    if (!_proto_tsig_calc(buf, *len, data, digest))
        return 0;

    tsig = orig = buf + *len;
    memcpy(tsig, data->name, data->namelen);
    tsig += data->namelen;

    *(unsigned short *)tsig = htons(TYPE_TSIG);
    *(unsigned short *)(tsig + 2) = htons(data->class);
    *(unsigned *)(tsig + 4) = htonl(data->ttl);
    *(unsigned short *)(tsig + 8) = htons(data->algolen + 16 + MD5_DIGEST);

    memcpy(tsig + 10, data->algo, data->algolen);
    tsig += 10 + data->algolen;

    *(unsigned short *)tsig = 0;
    *(unsigned *)(tsig + 2) = htonl(data->time);
    *(unsigned short *)(tsig + 6) = htons(data->fudge);
    *(unsigned short *)(tsig + 8) = htons(MD5_DIGEST);
    memcpy(tsig + 10, digest, MD5_DIGEST);
    tsig += 10 + MD5_DIGEST;

    *(unsigned short *)tsig = h->id;
    *(unsigned *)(tsig + 2) = 0; /* two birds with one stone */

    *len += tsig + 6 - orig;

    return 1;
}
