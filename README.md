rdnsD
=====

A lightweight DNS reflector that accepts DNS queries and re-issues them (secured using TSIG)

Build
=====

To build, on BSD or Linux, simply running make should work. For Solaris, use
Makefile.sunos by running make -f Makefile.sunos.

The resulting binary (rdnsD) should be put somewhere sensible for binaries,
we recommend /usr/local/bin

The command line options are documented by running with -h. One thing to note
is that if the daemon forks into the background (which it does by default)
logging will be disabled. To log the output to a file, see the -o option.

Note that logging at a level lower than INFO will produce vast amounts of
output. NOIS is almost completely worthless unless you're trying to debug
low-level packet issues.

Building an RPM
===============

If you need a CentOS 6.7 VM for the build, cd into the vagrant subdirectory
and run:

```
make up
```

Note that this is currently configured to use the VMWare Fusion provider
when building the VM. The previous command will display the commands
required to login to the VM, build the RPM, install the newly built RPM
locally, configure, and start the process.

```
scp Krdnsd-key.+157+58869.private deploy@${IP}:
ssh deploy@${IP}
git clone ssh://github.vrsn.com/djames/rdnsD.git
cd rdnsD
make rpm
sudo yum install -y ./rdnsD-*.rpm
sudo chkconfig rdnsD on
sudo mv /home/deploy/Krdnsd-key.+157+58869.private /etc/rdnsD
sudo perl -pi -e 's,"$, -k /etc/rdnsD/Krdnsd-key.+157+58869.private", if (/flags/);' /etc/rdnsD/rdnsD.conf
sudo service rdnsD start
```

Installing with Ansible
=======================

Testing locally:
```
ssh-agent bash
ssh-add ~/.ssh/id_rsa
ansible-playbook cto-edge.yml -i sites/local --ask-vault-pass --become --ask-become-pass
```

```
ssh-agent bash
ssh-add ~/.ssh/id_rsa
ansible-playbook cto-edge.yml -i sites/cto-edge --ask-vault-pass --become --ask-become-pass
```

Restarting secspider with Ansible
=================================

```
ansible -m service -a 'name=rdnsD state=restarted' -i sites/cto-edge --become --ask-become-pass secspider
```

Adding a new host to SecSpider
==============================
# Add edge_addr property to the host in sites/cto-edge
# Add the name of the host to the secspider group in sites/cto-edge
# Check current state
```
/sbin/ifconfig
/sbin/ip route
cat /etc/sysconfig/network
```
# Run the cto-net.yml playbook for the new host
```
ansible-playbook cto-net.yml -i sites/cto-edge --become --ask-become-pass -l ${new-host}
```
# Check updated files
```
cat /etc/sysconfig/{network,network-scripts/route-eth1,network-scripts/ifcfg-eth0.110}
```
# Restart networking on the new host
```
ansible -m service -a 'name=network state=restarted' -i sites/cto-edge --become --ask-become-pass ${new-host}
```
# Install SecSpider (as above)

```
ansible-playbook cto-edge.yml -i sites/cto-edge --ask-vault-pass --become --ask-become-pass
```

Test status of secspider
========================

```
ansible/bin/secspider-check.pl --sites=ansible/sites/cto-edge --tsig=***secret***
```

Run
===

Build and install rdnsD

Generate a TSIG key in order to secure communications between your source and your rdnsD poller. The dnssec-keygen utility is included in the bind packages.

Example:
```
dnssec-keygen -a HMAC-MD5 -b 128 -n HOST rdnsD-key
```

Example output would be files named (the numbers vary by key):

```
KrdnsD-key.+157+64252.key
KrdnsD-key.+157+64252.private
```

Usage for the poller can be obtained by running:
```
rdnsD -h
```

Example usage to run in daemon mode, on port 1337, with a rotating log file rdnsD.log and using the TSIG key 
generated above:

```
rdnsD -k $HOME/keys/KrdnsD-key.+157+64252.private -p 1337 -o $HOME/logs/rdnsD.log -d CRIT
```

Command Line Options
====================
Usage: ./rdnsD -k <key> [OPTIONS]<br>
Acts as a DNS forwarder

Options:<br>
-k <key>       key file (required)<br>
-l <addr>      listen address (default 0.0.0.0)<br>
-p <port>      listen port (default 53)<br>
-d <level>     debug level (default CRIT)<br>
-n <num>       max number of pending queries (default 800)<br>
-t <num>       max number of TCP queries (default 80)<br>
-o <file>      log output to file<br>
-s <size>      Max log size before rotating (default 31457280)<br>
-N <num>       Number of backup log files (default 5)<br>
-u <user>      switch to user after the socket is opened<br>
-f             stay in foreground<br>
-h             this message<br>

Debug levels: (each level incorporates those above it)<br>
NONE           totally quiet<br>
CRIT           only critical errors (default)<br>
INFO           informational notes, nothing serious<br>
WARN           mostly irrelevant stuff<br>
NOIS           only developers want to know this<br>
