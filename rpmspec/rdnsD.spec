Name:    rdnsD
Summary: rdnsD is a process that will reflect authorized DNS queries
Version: %{RPMVER}
License: Any
Release: %{RPMREV}%{?dist}
Group:   Applications/System
Source:  https://github.vrsn.com/eosterweil/rdnsD/rdnsD.tar.gz
URL:     https://github.vrsn.com/eosterweil/rdnsD

%description
rdnsD is a process that will reflect authorized DNS queries

%prep
%setup -q

%build
make

%install
make install DESTDIR=$RPM_BUILD_ROOT

%files
%attr(755, root, root) %{_bindir}/rdnsD
%attr(755, root, root) %dir %{_sysconfdir}/rdnsD
%attr(644, root, root) %config %{_sysconfdir}/rdnsD/rdnsD.conf
%attr(755, root, root) %config %{_sysconfdir}/init.d/rdnsD
