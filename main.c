#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>

#include "rdnsd.h"
#include "b64.h"

/* rdnsD was written by Michael Ryan <michaelj@seas.ucla.edu>
   it was based on ideas from Dan Massey's group at Colorado State University
   please send all problems, bug reports, flames, etc. to the author */

/* For Stephanie,
   who decided one day
   after only twenty one years
   that living
   just isn't all it's cracked up to be

   Hope it's better wherever you are */

unsigned char key[MAX_KEYLEN];
int keylen = 0;
int lru_max = DEFAULT_PENDING;
int tcp_max_conn = DEFAULT_TCP;
int exit_code = 0;
int update_resolver = 0; /* set to 1 if we receive HUP */

static int debug_level = CRIT;
static FILE *dbg_out;
static pthread_mutex_t out_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct rdnsd_log_ctx_s g_tLogCtx;

FILE *_log_rotate(FILE *p_pStream, struct rdnsd_log_ctx_s *p_pLogCtx)
{
  FILE *pRet = p_pStream;
  struct stat tStat;
  int i = 0;
  char szBakFile1[_MAX_FILE_NAME + 1];
  char szBakFile2[_MAX_FILE_NAME + 1];

  if (NULL == p_pStream)
  {
    fprintf(stderr, "Unable to rotate with NULL stream.");
  }
  else if (NULL == p_pLogCtx)
  {
    fprintf(stderr, "Unable to rotate with NULL context.");
  }
  else if (NULL != p_pLogCtx->m_szFile)
  {
    memset(&tStat, 0, sizeof(tStat));
    if (-1 == stat(p_pLogCtx->m_szFile, &tStat))
    {
      fprintf(stderr, "Unable to stat log file: %s", p_pLogCtx->m_szFile);
    }
    else if (tStat.st_size >= p_pLogCtx->m_uMaxSize)
    {
      for (i = p_pLogCtx->m_iMaxBakFiles - 2; i >= 0; i--)
      {
        memset(szBakFile1, 0, _MAX_FILE_NAME + 1);
        memset(szBakFile2, 0, _MAX_FILE_NAME + 1);
        snprintf(szBakFile1, _MAX_FILE_NAME, "%s.%d", p_pLogCtx->m_szFile, i + 1);
        snprintf(szBakFile2, _MAX_FILE_NAME, "%s.%d", p_pLogCtx->m_szFile, i);

        if (-1 != stat(szBakFile1, &tStat))
        {
          unlink(szBakFile1);
        }

        if (-1 != stat(szBakFile2, &tStat))
        {
          if (-1 == link(szBakFile2, szBakFile1))
          {
            fprintf(stderr, "Unable to rotate log file.");
          }
        }
        unlink(szBakFile2);
      }

      fclose(p_pStream);
      memset(szBakFile1, 0, _MAX_FILE_NAME + 1);
      snprintf(szBakFile1, _MAX_FILE_NAME, "%s.%d", p_pLogCtx->m_szFile, i + 1);
      if (-1 == link(p_pLogCtx->m_szFile, szBakFile1))
      {
        fprintf(stderr, "Unable to roll main file: %s", strerror(errno));
      }
      unlink(p_pLogCtx->m_szFile);

      if (NULL == (pRet = fopen(p_pLogCtx->m_szFile, "w")))
      {
        fprintf(stderr, "Unable to re-open log file.");
      }
    }
  }

  return pRet;
}

/* print the date, used by err() and debug() */
static void _ddate(void) {
    time_t now;
    char buf[24];

    now = time(NULL);
    strftime(buf, 24, "%Y-%m-%d %T GMT", gmtime(&now));

    fprintf(dbg_out, "[%s] ", buf);
}

/* thread-safe
   based on OpenBSD err()
   copyright 1993 regents of the university of california
   standard 4-clause BSD licence omitted for brevity */

void err(int eval, char *fmt, ...) {
    va_list ap;
    int olderr;

    olderr = errno;

    pthread_mutex_lock(&out_mutex);

    va_start(ap, fmt);
    if (dbg_out != NULL) {
        _ddate();

        if (fmt != NULL) {
            vfprintf(dbg_out, fmt, ap);
            fprintf(dbg_out, ": ");
        }

        fprintf(dbg_out, "%s (fatal)\n", strerror(olderr));
        fflush(dbg_out);
    }
    va_end(ap);

    exit_code = eval;
    pthread_mutex_unlock(&out_mutex);
}

void debug(int level, char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    if (debug_level <= level && dbg_out != NULL) {
        pthread_mutex_lock(&out_mutex);

        dbg_out = _log_rotate(dbg_out, &g_tLogCtx);
        _ddate();

        vfprintf(dbg_out, fmt, ap);
        fprintf(dbg_out, "\n");

        /* ensure the data is written to the log immediately */
        fflush(dbg_out);

        pthread_mutex_unlock(&out_mutex);
    }
    va_end(ap);
}

int key_load(char *fn) {
    FILE *f;
    char buf[BUFSIZ];

    if ((f = fopen(fn, "r")) == NULL) {
        debug(CRIT, "couldn't open %s: %s", fn, strerror(errno));
        return 0;
    }

    if (fgets(buf, BUFSIZ, f) == NULL || fgets(buf, BUFSIZ, f) == NULL) {
        fclose(f);
        debug(CRIT, "not enough lines in key file");
        return 0;
    }
    if (strncmp(buf, "Algorithm: 157", 14) != 0) {
        fclose(f);
        debug(CRIT, "not the correct algorithm");
        return 0;
    }
    if (fgets(buf, BUFSIZ, f) == NULL) {
        fclose(f);
        debug(CRIT, "not enough lines in key file");
        return 0;
    }
    fclose(f);

    if ((keylen = decode((unsigned char *)(buf + 4), key, strlen(buf),
                    MAX_KEYLEN)) < 0) {
        debug(CRIT, "key too long");
        return 0;
    }
    --keylen;

    return 1;
}

void usage(char *name) {
    printf("Usage: %s -k <key> [OPTIONS]\n"
            "Acts as a DNS forwarder\n"
            "\n"
            "Options:\n"
            "-k <key>       key file (required)\n"
            "-l <addr>      listen address (default 0.0.0.0)\n"
            "-p <port>      listen port (default 53)\n"
            "-d <level>     debug level (default CRIT)\n"
            "-n <num>       max number of pending queries (default %d)\n"
            "-t <num>       max number of TCP queries (default %d)\n"
            "-o <file>      log output to file\n"
            "-s <size>      Max log size before rotating (default 31457280)\n"
            "-N <num>       Number of backup log files (default 5)\n"
            "-u <user>      switch to user after the socket is opened\n"
            "-f             stay in foreground\n"
            "-h             this message\n"
            "\n"
            "Debug levels: (each level incorporates those above it)\n"
            "NONE           totally quiet\n"
            "CRIT           only critical errors (default)\n"
            "INFO           informational notes, nothing serious\n"
            "WARN           mostly irrelevant stuff\n"
            "NOIS           only developers want to know this\n"
            "\n"
            "rdnsD is 2 kewl 4 skewl\n",
            name, DEFAULT_PENDING, DEFAULT_TCP);
}

/* get new default resolver on SIGHUP */
void sighup_handler(int signo) {
    update_resolver = 1;
}

/* kill signal handler */
static void _kill_handler(int signo) {
    debug(INFO, "caught signal %d", signo);
    exit_code = 1;
}

static int _daemonise(void) {
    pid_t pid;
    int fd;

    /* fork to the background */
    if ((pid = fork()) < 0) {
        err(1, "fork");
        return -1;
    }
    if (pid > 0)
        return pid;
    if (dbg_out == stderr)
        dbg_out = NULL;

    /* reassociate */
    if (setsid() < 0) {
        err(1, "setsid");
        return -1;
    }

    /* close and reopen fds */
    close(0);
    close(1);
    close(2);
    fd = open("/dev/null", O_RDWR);
    dup(fd);
    dup(fd);

    /* chdir */
    if (chdir("/") < 0) {
        err(1, "fork");
        return -1;
    }

    return 0;
}

int save_pid(const char* pid_file) {
  int r = -1;
  FILE* pf = fopen(pid_file, "w");

  if (pf != NULL) {
    fprintf(pf, "%d\n", getpid());
    fclose(pf);
    r = 0;
  }

  return r;
}

int main(int argc, char **argv) {
    int fd, tfd, opt;
    struct in_addr addr;
    int port, background;
    struct passwd *new_uid;
    pthread_t tcp_thread;
    char* pid_file = NULL;

    dbg_out = stderr; /* default debug goes to stderr */

    addr.s_addr = INADDR_ANY;
    port = 53;
    background = 1;
    new_uid = NULL;

    signal(SIGHUP, sighup_handler);
    signal(SIGPIPE, SIG_IGN); /* we handle these inline */
    signal(SIGTERM, _kill_handler);
    signal(SIGINT, _kill_handler);

    memset(&g_tLogCtx, 0, sizeof(g_tLogCtx));
    g_tLogCtx.m_uMaxSize = 30 * 1024 * 1024;
    g_tLogCtx.m_iMaxBakFiles = 5;

    while ((opt = getopt(argc, argv, "l:p:d:fk:o:u:hn:t:s:N:P:")) > 0) {
        switch (opt) {
            case 'l':
                if (!inet_aton(optarg, &addr)) {
                    printf("invalid address: %s\n", optarg);
                    usage(argv[0]);
                    return 1;
                }
                break;
            case 'p':
                if ((port = atoi(optarg)) <= 0 || port > 65535) {
                    printf("invalid port: %s\n", optarg);
                    usage(argv[0]);
                    return 1;
                }
                break;
            case 'd':
                if (strcmp(optarg, "NOIS") == 0)
                    debug_level = NOIS;
                else if (strcmp(optarg, "WARN") == 0)
                    debug_level = WARN;
                else if (strcmp(optarg, "INFO") == 0)
                    debug_level = INFO;
                else if (strcmp(optarg, "CRIT") == 0)
                    debug_level = CRIT;
                else if (strcmp(optarg, "NONE") == 0)
                    debug_level = NONE;
                else {
                    printf("invalid debug level: %s\n", optarg);
                    usage(argv[0]);
                    return 1;
                }
                break;
            case 'f':
                background = 0;
                break;
            case 'k':
                if (!key_load(optarg)) {
                    printf("couldn't load key\n");
                    return 1;
                }
                break;
            case 'o':
                if ((dbg_out = fopen(optarg, "a")) == NULL) {
                    printf("couldn't open %s to log\n", optarg);
                    return 1;
                }
                g_tLogCtx.m_szFile = optarg;
                break;
            case 'u':
                if ((new_uid = getpwnam(optarg)) == NULL) {
                    printf("Couldn't find user %s\n", optarg);
                    return 1;
                }
                break;
            case 'h':
                usage(argv[0]);
                return 0;
            case 'n':
                if ((lru_max = atoi(optarg)) <= 0) {
                    printf("Maximum pending must be > 0\n");
                    return 1;
                }
                break;
            case 't':
                if ((tcp_max_conn = atoi(optarg)) <= 0) {
                    printf("Maximum TCP must be > 0\n");
                    return 1;
                }
                break;
            case 's':
                if (atoi(optarg) <= 0)
                {
                  printf("Unable to parse logfile size: '%s'\n", optarg);
                  return 1;
                }
                else
                {
                  g_tLogCtx.m_uMaxSize = (size_t) atoi(optarg);
                }
                break;
            case 'N':
                if ((g_tLogCtx.m_iMaxBakFiles = atoi(optarg)) < 0)
                {
                  printf("Unable to parse: num back log files '%s'\n", optarg);
                  return 1;
                }
                break;
            case 'P':
                pid_file = optarg;
                break;
            case '?':
            default:
                return 1;
        }
    }

    if (keylen == 0) {
        printf("no key given\n");
        usage(argv[0]);
        return 1;
    }

    if (!query_default_resolver())
        return 1;

    if ((fd = net_socket_bind(SOCK_DGRAM, addr, port)) < 0
            || (tfd = net_socket_bind(SOCK_STREAM, addr, port)) < 0)
        return 1;

    /* cannot forget to call listen on the TCP socket */
    if (listen(tfd, 10) < 0) {
        err(1, "listen");
        return 1;
    }

    if (background) {
        pid_t pid;

        /* quit if we can't become a daemon (_daemonise() prints the error) */
        if ((pid = _daemonise()) < 0)
            return 1;

        /* parent */
        else if (pid > 0)
            return 0;

        if (pid_file != NULL) {
          if (save_pid(pid_file) != 0) {
            err(1, "cannot write pid to %s", pid_file);
            return 1;
          }
        }

        /* drop privs if applicable */
        if (new_uid != NULL)
            if (setuid(new_uid->pw_uid) < 0) {
                err(1, "cannot switch to user id %d", new_uid->pw_uid);
                return 1;
            }

        /* otherwise we're the child */
    }

    /* spawn TCP thread */
    if (pthread_create(&tcp_thread, NULL, tcp_net_loop, &tfd) != 0) {
        debug(CRIT, "could not spawn TCP thread");
        return 1;
    }

    /* do work for UDP thread */
    net_loop(fd);

    /* clean up all memory */
    query_cleanup();
    net_cleanup();

    /* join the TCP thread, which will shortly die */
    pthread_join(tcp_thread, NULL);

    if (dbg_out != NULL)
        fclose(dbg_out);

    return exit_code;
}
