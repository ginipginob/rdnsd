#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h> /* for strerror */
#include <unistd.h>
#include <sys/poll.h> /* for error constants */
#include <sys/socket.h>
#include <arpa/inet.h>

#include "rdnsd.h"
#include "llist.h"

extern struct in_addr def_res;
extern int query_avail;
extern int exit_code;

static LL_HEAD(t_query_list_head);

struct _tcp_query_t {
    int cfd, sfd; /* client fd and remote server fd */
    fd_entry_t cfdi, sfdi; /* index in poll fd table */
    struct sockaddr_in sin; /* keep this around for no good reason */
    struct in_addr remote; /* remote IP address, kept for logging */

    char *buffer;
    int wait_bytes;

    unsigned short pkt_size;

    /* keep tsig data */
    tsig_data_t *tsig;
    int tsig_size;

    struct llhead t_query_list;
};

/* pants pants pants pants pants pants */

/* a few useful prototypes */
static int _tcp_query_read(int, tcp_query_t *);
static int _tcp_query_read_short(int, tcp_query_t *);
static int _tcp_query_write(int, tcp_query_t *);
static int _tcp_query_write_short(int, tcp_query_t *);
static int _tcp_query_close_fd(int, fd_entry_t);

/* moves a query to the front of the list */
void _tcp_query_touch(tcp_query_t *q) {
    LL_MOVE_TO_HEAD(&t_query_list_head, &q->t_query_list);
}

/* aborts a query on error */
static int _tcp_query_socket_error(int fd, int err, tcp_query_t *q) {
    assert(fd >= 0);
    assert(q != NULL);

    if (err & POLLERR)
        debug(WARN, "error condition on socket");

    /* more verbose error handling can be done using ancillary data */
    if (err & POLLHUP)
        debug(WARN, "socket hang up");

    /* we should never get this */
    if (err & POLLNVAL) {
        debug(CRIT, "socket not open");
        exit_code = 1;
        return -1;
    }

    return tcp_query_destroy(q);
}

/* finished writing a whole packet, transition state */
static int _tcp_query_complete_write(int fd, tcp_query_t *q) {
    assert(fd >= 0);
    assert(q != NULL);

    /* finished writing the response, so just quit immediately */
    if (fd == q->cfd) {
        debug(WARN, "finished writing response to %s",
                inet_ntoa(q->sin.sin_addr));
        return tcp_query_destroy(q);
    }

    debug(WARN, "finished writing question to %s", inet_ntoa(q->remote));

    /* finished writing the question to the server, so wait to read */
    q->wait_bytes = 2;
    tcp_net_read_cb(q->sfdi, _tcp_query_read_short);

    /* get rid of the read buffer while we're here */
    free(q->buffer);
    q->buffer = NULL;

    /* data may be immediately available */
    return _tcp_query_read_short(q->sfd, q);
}

/* write a packet */
static int _tcp_query_write(int fd, tcp_query_t *q) {
    ssize_t w;

    assert(fd >= 0);
    assert(q != NULL);

    while ((w = send(fd, q->buffer + (q->pkt_size - q->wait_bytes),
                    q->wait_bytes, 0)) != 0) {
        if (w < 0) {
            if (errno == EINTR) {
                debug(INFO, "send system call interrupted");
                continue;
            }

            if (errno == EAGAIN || errno == EWOULDBLOCK)
                return 0;

            if (errno == EPIPE)
                break;

            debug(INFO, "dropping TCP query on send: %s", strerror(errno));
            return tcp_query_destroy(q);
        }

        debug(NOIS, "wrote %d bytes", w);

        _tcp_query_touch(q);

        if ((q->wait_bytes -= w) == 0)
            return _tcp_query_complete_write(fd, q);
    }

    debug(WARN, "socket closed when writing to %s", inet_ntoa(q->remote));
    return tcp_query_destroy(q);
}

/* write the short and as much of the packet as we can */
static int _tcp_query_write_short(int fd, tcp_query_t *q) {
    struct msghdr msg;
    struct iovec iov[2];
    ssize_t w;

    assert(fd >= 0);
    assert(q != NULL);

    /* set the fields of msg */
    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = iov;
    msg.msg_iovlen = 2;
#ifdef SUNOS
    msg.msg_accrights = NULL;
    msg.msg_accrightslen = 0;
#else
    msg.msg_control = NULL;
    msg.msg_controllen = 0;
    msg.msg_flags = 0;
#endif /* SUNOS */

    /* cast to void * to avoid arithmetic problems */
    iov[0].iov_base = (void *)&q->pkt_size + (2 - q->wait_bytes);
    iov[0].iov_len = q->wait_bytes;

    /* load the packet for kicks */
    iov[1].iov_base = q->buffer;
    iov[1].iov_len = q->pkt_size;

    /* need this to be in net order */
    q->pkt_size = htons(q->pkt_size);

    while ((w = sendmsg(fd, &msg, 0)) != 0) {
        if (w < 0) {
            /* interrupted system call */
            if (errno == EINTR) {
                debug(INFO, "sendmsg system call interrupted");
                continue;
            }

            /* would block, so quit it */
            if (errno == EAGAIN || errno == EWOULDBLOCK)
                return 0;

            /* socket closed */
            if (errno == EPIPE)
                break;

            /* some other error: drop query */
            debug(INFO, "dropping query on sendmsg: %s", strerror(errno));
            return tcp_query_destroy(q);
        }

        debug(NOIS, "wrote %d bytes to socket", w);

        _tcp_query_touch(q);

        /* wrote the entire short */
        if (w >= q->wait_bytes) {
            /* don't need this in network byte order anymore */
            q->pkt_size = htons(q->pkt_size);

            /* skip past the part we've written */
            w -= q->wait_bytes;

            /* we may have immediately completed the write */
            if ((q->wait_bytes = q->pkt_size - w) == 0)
                return _tcp_query_complete_write(fd, q);

            /* set the proper callback */
            tcp_net_write_cb(fd == q->cfd ? q->cfdi : q->sfdi,
                    _tcp_query_write);

            /* write the rest of the packet */
            return _tcp_query_write(fd, q);
        }

        /* try again */
        iov[0].iov_base += w;
        iov[1].iov_len -= w;
    }

    debug(WARN, "socket closed when writing to %s", inet_ntoa(q->remote));
    return tcp_query_destroy(q);
}

/* test to see if a connect worked */
static int _tcp_query_connect(int fd, tcp_query_t *q) {
    int errc;
    socklen_t err_size;

    assert(q != NULL);

    /* test to see if the connect succeeded */
    err_size = sizeof(errc);
    if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &errc, &err_size) < 0) {
        debug(CRIT, "dropping query on getsockopt: %s", strerror(errno));
        return tcp_query_destroy(q);
    }

    /* the connect succeeded */
    if (errc == 0) {
        debug(WARN, "connected to %s", inet_ntoa(q->remote));

        _tcp_query_touch(q);

        /* write the length and packet when writing is available */
        q->wait_bytes = 2;
        tcp_net_write_cb(q->sfdi, _tcp_query_write_short);

        /* it may work right away */
        return _tcp_query_write_short(q->sfd, q);
    }

    /* there was a failure, so log it at WARN and destroy this query */
    debug(WARN, "connect to %s failed: %s", inet_ntoa(q->remote),
            strerror(errc));

    /* get rid of the query, which can fail (due to close) */
    return tcp_query_destroy(q);
}

/* complete a read from the client or the server */
static int _tcp_query_complete_read(int rfd, tcp_query_t *q) {
    int fd, opt, tsig, opt_ret;
    size_t len;
    dnshead_t *h;

    assert(rfd >= 0);
    assert(q != NULL);

    /* finished reading the remote response, sign and return to client */
    if (rfd == q->sfd) {
        int count;
        size_t len;

        debug(WARN, "finished reading query response from %s",
                inet_ntoa(q->remote));

        /* <EMO> 11-18-2015
        q->remote = q->sin.sin_addr;
        */
        q->remote.s_addr = 0;

        /* it really sucks that close can fail */
        if ((count = _tcp_query_close_fd(q->sfd, q->sfdi)) < 0)
            return -1;
        q->sfd = -1;

        /* add the TSIG to the packet */
        len = q->pkt_size;
        if (!proto_tsig_add(q->buffer, &len, q->tsig))
            return -1;

        /* adjust the packet length to include the TSIG */
        assert(len <= 0xffffu);
        assert(len == q->pkt_size + q->tsig_size);
        q->pkt_size = len;

        /* wait for a short */
        q->wait_bytes = 2;
        tcp_net_write_cb(q->cfdi, _tcp_query_write_short);
        return count + _tcp_query_write_short(q->cfd, q);
    }

    debug(WARN, "finished reading query from %s", inet_ntoa(q->sin.sin_addr));

    /* all this logic is pretty similar to query.c */

    /* parse the packet */
    if (!proto_opt_tsig(q->buffer, q->pkt_size, &opt, &tsig)) {
        debug(WARN, "packet has no OPT or TSIG record");
        return tcp_query_destroy(q);
    }

    /* make sure it has a tsig */
    if (tsig < 0) {
        debug(CRIT, "received packet without TSIG from %s",
                inet_ntoa(q->sin.sin_addr));
        return tcp_query_destroy(q);
    }

    /* exit on allocation failure */
    len = q->pkt_size;
    if (!proto_tsig_verify_strip(q->buffer, &len, tsig, &q->tsig)) {
      debug(CRIT, "unable to verify TCP TSIG from %s", inet_ntoa(q->sin.sin_addr));
      return -1;
    }

    /* abort if there's an invaild tsig */
    if (q->tsig == NULL) {
      h = (dnshead_t *)q->buffer;
        debug(CRIT, "received TCP packet with counts (qd %d, an %d, ns %d, ar %d) with invalid TSIG from %s",
            h->qdcount,
            h->ancount,
            h->nscount,
            h->arcount,
            inet_ntoa(q->sin.sin_addr));
        return tcp_query_destroy(q);
    }

    if (!(opt_ret = proto_opt_parse(q->buffer, &len, opt, &q->remote))) {
        debug(CRIT, "error or kill in OPT from %s",
                inet_ntoa(q->sin.sin_addr));
        return tcp_query_destroy(q);
    }

    q->pkt_size = len;

    /* get rid of the opt record if necessary */
    if (opt_ret == PROTO_STRIP) {
        h = (dnshead_t *)q->buffer;
        h->arcount = htons(ntohs(h->arcount) - 1);
    }

    /* default resolver */
    if (!q->remote.s_addr)
        q->remote = def_res;

    debug(WARN, "attempting to connect to %s:53", inet_ntoa(q->remote));

    /* can't connect, so give up */
    if ((fd = net_socket_connect(SOCK_STREAM, q->remote, 53)) < 0) {
        debug(WARN, "cannot connect to %s", inet_ntoa(q->remote));
        return tcp_query_destroy(q);
    }

    q->sfd = fd;
    if ((q->sfdi = tcp_net_new_fd(fd, q, _tcp_query_socket_error)) < 0)
        return -1;
    tcp_net_write_cb(q->sfdi, _tcp_query_connect);

    /* don't need this for now, but keep checking for errors */
    tcp_net_clear_cb(q->cfdi);

    return 0;
}

/* read the entirety of the packet, transitioning state
   if we read from a client, we need to verify tsig, then connect to a remote
   if we read from a server, we need to add a tsig and send to the client */
static int _tcp_query_read(int fd, tcp_query_t *q) {
    ssize_t r;

    assert(fd >= 0);
    assert(q != NULL);

    while ((r = recv(fd, q->buffer + (q->pkt_size - q->wait_bytes),
                    q->wait_bytes, 0)) != 0) {
        if (r < 0) {
            if (errno == EINTR) {
                debug(NOIS, "recv system call interruped");
                continue;
            }

            if (errno == EAGAIN || errno == EWOULDBLOCK)
            {
                debug(NOIS, "recv system call EAGAIN");
                return 0;
            }

            if (errno == EPIPE || errno == ECONNRESET)
            {
                debug(NOIS, "recv system call EPIPE");
                break;
            }

            debug(INFO, "dropping query on recv: %s", strerror(errno));
            return tcp_query_destroy(q);
        }

        debug(NOIS, "read %d bytes from socket", r);

        /* finished reading a query, so transition state */
        if ((q->wait_bytes -= r) == 0)
            return _tcp_query_complete_read(fd, q);
    }

    /* read 0, so gtfo */
    debug(WARN, "socket closed when reading");
    return tcp_query_destroy(q);
}

/* read a short off the socket, then transition state as necessary */
static int _tcp_query_read_short(int fd, tcp_query_t *q) {
    struct msghdr msg;
    struct iovec iov[2];
    char buf[1024]; /* try to read in a modestly large packet */
    ssize_t r;

    assert(fd >= 0);
    assert(q != NULL);

    /* set the basic fields */
    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = iov;
    msg.msg_iovlen = 2;
#ifdef SUNOS
    msg.msg_accrights = NULL;
    msg.msg_accrightslen = 0;
#else
    msg.msg_control = NULL;
    msg.msg_controllen = 0;
    msg.msg_flags = 0;
#endif /* SUNOS */

    iov[0].iov_base = (void *)&q->pkt_size + (2 - q->wait_bytes);
    iov[0].iov_len = q->wait_bytes;

    /* read past the short if we can */
    iov[1].iov_base = buf;
    iov[1].iov_len = sizeof(buf);

    /* it slices, it dices, it reads into multiple buffers! */
    while ((r = recvmsg(fd, &msg, 0)) != 0) {
        if (r < 0) {
            if (errno == EINTR) {
                debug(INFO, "recvmsg system call interrupted");
                continue;
            }

            if (errno == EAGAIN || errno == EWOULDBLOCK)
                return 0;

            /* connection closed */
            if (errno == EPIPE || errno == ECONNRESET)
                break;

            debug(INFO, "dropping query on recvmsg: %s", strerror(errno));
            return tcp_query_destroy(q);
        }

        debug(NOIS, "read %d bytes from socket", r);

        _tcp_query_touch(q);

        /* read at least as much as the short */
        if (r >= q->wait_bytes) {
            r -= q->wait_bytes;
            q->pkt_size = ntohs(q->pkt_size);

            /* make sure the packet has a header at least */
            if (q->pkt_size < sizeof(dnshead_t)) {
                debug(WARN, "packet too short");
                return tcp_query_destroy(q);
            }

            /* include space for tsig in packet if necessary */
            if (q->tsig != NULL)
                q->tsig_size = proto_tsig_size(q->tsig);

            /* corner case: if the packet will be > 64k drop it */
            if (q->pkt_size + q->tsig_size > 0xffffu) {
                debug(CRIT, "TSIG makes packet too large");
                return tcp_query_destroy(q);
            }

            /* corner case: if we read more than the packet holds (malformed), drop it */
            if (q->pkt_size + q->tsig_size < r) {
                debug(CRIT, "rdnsD read more bytes than the message holds, malformed?");
                return tcp_query_destroy(q);
            }

            /* allocate space for packet + tsig */
            if ((q->buffer = malloc(q->pkt_size + q->tsig_size)) == NULL) {
                err(1, "malloc");
                return -1;
            }
            debug(NOIS, "just alloc'ed %d + %d bytes, now memcpy'ing %d", q->pkt_size, q->tsig_size, r);

            /* copy in the data we've already read */
            memcpy(q->buffer, buf, r);

            /* test to see if we've read the whole thing */
            if ((q->wait_bytes = q->pkt_size - r) == 0)
                return _tcp_query_complete_read(fd, q);

            debug(NOIS, "still need %d bytes", q->wait_bytes);

            /* read the DNS packet */
            tcp_net_read_cb(fd == q->cfd ? q->cfdi : q->sfdi,
                    _tcp_query_read);

            /* read rest */
            return _tcp_query_read(fd, q);
        }

        /* try again */
        iov[0].iov_base += r;
        iov[1].iov_len -= r;
    }

    /* there was a read of zero, so get rid of this query */
    debug(WARN, "socked closed when reading");
    return tcp_query_destroy(q);
}

/* create a new query */
int tcp_query_new(int fd, struct sockaddr_in *sin) {
    tcp_query_t *q, *n;
    int count;

    assert(sin != NULL);

    count = 0;

    /* remove the least recently used connection */
    if (query_avail == 0) {
        int r;

        n = tcp_query_lru();
        assert(n != NULL);

        debug(WARN, "dropping TCP query for %s (LRU)",
                inet_ntoa(n->sin.sin_addr));

        /* close really sucks. it should never fail. BUT IT MAY! */
        if ((r = tcp_query_destroy(n)) < 0)
            return -1;
        count += r;
    }

    assert(query_avail >= 1);

    if ((q = malloc(sizeof(tcp_query_t))) == NULL) {
        err(1, "malloc");
        return -1;
    }
    memset(q, 0, sizeof(tcp_query_t));

    /* copy in this useless structure */
    q->sin = *sin;

    /* add the read fd to the fd list */
    q->cfd = fd;
    if ((q->cfdi = tcp_net_new_fd(fd, q, _tcp_query_socket_error)) < 0)
        return -1;
    q->wait_bytes = 2;
    tcp_net_read_cb(q->cfdi, _tcp_query_read_short);

    /* don't know the remote server yet */
    q->sfd = -1;
    q->sfdi = -1;

    /* in case of error condition, we need to know if we need to free this */
    q->buffer = NULL;
    q->tsig = NULL;
    q->tsig_size = 0;

    /* wait for a short (it holds the packet size) */
    q->wait_bytes = 2;

    /* add this biatch to the list */
    LL_ADD(&t_query_list_head, &q->t_query_list);

    /* count down the number of available queries */
    --query_avail;

    return count + _tcp_query_read_short(fd, q);
}

/* fetches the least recently used query */
tcp_query_t *tcp_query_lru(void) {
    if (LL_EMPTY(&t_query_list_head))
        return NULL;

    return LL_ENTRY(t_query_list_head.prev, tcp_query_t, t_query_list);
}

/* close a file descriptor
   returns -1 on failure, boolean if the fd was interesting */
int _tcp_query_close_fd(int fd, fd_entry_t fdi) {
    int r;

    if (fd >= 0) {
        while ((r = close(fd)) < 0 && errno == EINTR)
            debug(INFO, "close system call interrupted");

        if (r < 0)
            debug(CRIT, "close system call failed: %s", strerror(errno));

        debug(NOIS, "closed fd %d", fd);

        if (fdi >= 0) {
            debug(NOIS, "removed fdi %d", fdi);
            return tcp_net_remove_fd(fdi) ? 1 : 0;
        }
    }

    return 0;
}

/* destroy the query, close the connections, and free up memory
   returns -1 on failure, number of interesting FDs removed otherwise */
int tcp_query_destroy(tcp_query_t *q) {
    int count, r;

    assert(q != NULL);

    count = 0;

    /* remove the entry for the client fd and server fd */
    if ((r = _tcp_query_close_fd(q->cfd, q->cfdi)) < 0)
        return -1;
    count += r;
    if ((r = _tcp_query_close_fd(q->sfd, q->sfdi)) < 0)
        return -1;
    count += r;

    LL_DEL(&q->t_query_list);

    /* free up the memory */
    free(q->buffer);
    free(q->tsig);
    free(q);

    /* YAY MEMORY! */
    ++query_avail;

    return count;
}

/* clean up all memory used by this class */
int tcp_query_cleanup(void) {
    tcp_query_t *q;
    struct llhead *lp, *tmp;

    LL_FOREACH_SAFE(&t_query_list_head, lp, tmp) {
        q = LL_ENTRY(lp, tcp_query_t, t_query_list);
        LL_DEL(lp);
        debug(INFO, "destroying TCP query");

        /* because close can fail */
        if (tcp_query_destroy(q) < 0)
            return 0;
    }

    return 1;
}
